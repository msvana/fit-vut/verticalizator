# -*- coding: utf-8 -*-

# Author: Miloš Švaňa (xsvana01)
# Brno 2016

import re

class HtmlPreprocessor(object):
	anchor_re = '(<a [^>]*href=\"(.*?)\"[^>]*>(.*?)</a>)'
	img_re = '(<img [^>]*src=\"(.*?)\"[^>]*>)'
	links_re = re.compile('('+anchor_re+'|'+img_re+')', re.IGNORECASE)

	def __init__(self, old_html):
		self.old_html = old_html.decode('utf-8', 'ignore')
		self.new_html = u''
		self.processed_chars = 0
		self.links = []

	def preprocess(self):
		self.save_links()
		return self.new_html

	def get_links(self):
		return self.links

	def save_links(self):
		for link_match in self.links_re.finditer(self.old_html):
			self.process_link_match(link_match)
		self.new_html += self.old_html[self.processed_chars:]

	def process_link_match(self, link_match):
		if not link_match.group(2):
			self.process_image_match(link_match)
		else:
			self.process_anchor_match(link_match)
		self.processed_chars = link_match.end()

	def process_image_match(self, image_match):
		src = image_match.group(6)
		self.new_html += "__IMG__"
		self.links.append(("", src))

	def process_anchor_match(self, anchor_match):
		uri = anchor_match.group(3)
		text = anchor_match.group(4)
		if len(uri) > 0 and uri[0] == '#':
			self.new_html += text + " "
		else:
			self.new_html += self.old_html[self.processed_chars:anchor_match.start()]
			self.new_html += " __LINK__ "
			self.links.append((text, uri))
