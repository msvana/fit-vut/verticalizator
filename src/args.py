# -*- coding: utf-8 -*-

# Author: Miloš Švaňa (xsvana01)
# Brno 2016

from argparse import ArgumentParser

'''
Command line arguments parser
Usage example in python code:

arguments = Arguments()
input_file_path = arguments.get_input()
if arguments.is_input_html():
	input_file = open(input_file_path)
else:
	warcreader = WarcReader(input_file_path)
'''

PROGRAM_DESCRIPTION = 'verticalizes a WARC or HTML file'

INPUT_HELP = 'path to the input WARC (may be gzipped), HTML or WIKIPEDIA HTML file' \
             'reads WARC from STDIN if not set'

OUTPUT_HELP = 'path to the output VERT file'

NOLANGDETECT_HELP = 'turns off language detection, speeds up the verticalization but some ' \
                    'non english websites might be processed too'

WIKI_HELP = 'Input file contains wikipedia pages with HTML markup'

STOPWORDS = 'File containing a list of stop words which are used to extract important text'

class Arguments(object):

	def __init__(self):
		self.argparser = ArgumentParser(description=PROGRAM_DESCRIPTION)
		self.argparser.add_argument('-i', '--input', help=INPUT_HELP)
		self.argparser.add_argument('-o', '--output', help=OUTPUT_HELP, required=True)
		self.argparser.add_argument('-l', '--nolangdetect', help=NOLANGDETECT_HELP, action='store_true')
		self.argparser.add_argument('-w', '--wiki', help=WIKI_HELP, action='store_true')
		self.argparser.add_argument('-s', '--stopwords', help=STOPWORDS, required=True)
		self.parsed = False

	def get_input(self):
		self.parse()
		return self.args.input

	def is_input_warc(self):
		input_path = self.get_input()
		return input_path is None or input_path[-7:] == 'warc.gz' or input_path[-4:] == 'warc'

	def is_input_html(self):
		input_path = self.get_input()
		return input_path[-5:] == '.html'

	def is_input_wiki(self):
		self.parse()
		return self.args.wiki

	def get_stopwords(self):
		self.parse()
		return self.args.stopwords

	def get_output(self):
		self.parse()
		return self.args.output

	def is_langdetect_allowed(self):
		self.parse()
		return not self.args.nolangdetect

	def parse(self):
		if not self.parsed:
			self.args = self.argparser.parse_args()
			self.parsed = True

