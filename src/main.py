# -*- coding: utf-8 -*-

# Author: Miloš Švaňa (xsvana01)
# Brno 2016

import os, sys
from ctypes import cdll
libs_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
cdll.LoadLibrary(libs_path + '/libhtmlcxx.so.3')

from args import Arguments
from verticalizator import Verticalizator
from warcreader import WarcReader, Webpage
from wikireader import WikiReader
import codecs

def create_and_configure_verticalizator(arguments):
	verticalizator = Verticalizator(arguments.get_stopwords())
	verticalizator.allow_log()

	if arguments.is_langdetect_allowed() and not arguments.is_input_wiki():
		verticalizator.allow_langdetect()

	return verticalizator

def process_warc_file(verticalizator, input_path, output_file):
	warc_reader = WarcReader(input_path)

	for web_page in warc_reader.iterate():
		vert = verticalizator.verticalize(web_page)
		if vert:
			output_file.write(vert.decode(errors='replace'))

def process_wiki_file(verticalizator, input_path, output_file):
	verticalizator.keep_everything()

	wiki_reader = WikiReader(input_path)

	for wiki_page in wiki_reader.iterate():
		vert = verticalizator.verticalize(wiki_page)
		if vert:
			output_file.write(vert)

def process_html_file(verticalizator, input_path, output_file):
	html_file = open(input_path, 'r')
	payload = html_file.read()
	html_file.close()
	web_page = Webpage('local', payload, True)
	vert = verticalizator.verticalize(web_page)
	output_file.write(vert)

if __name__ == '__main__':
	arguments = Arguments()
	reload(sys).setdefaultencoding('utf8')
	
	print('Starting verticalization for file: %s' % arguments.get_input())
	if arguments.is_input_wiki():
		print('This file will be processed as wikipedia file')
	print('Output will be saved as file: %s' % arguments.get_output())

	output_file = codecs.open(arguments.get_output(), 'w', encoding='utf8')
	verticalizator = create_and_configure_verticalizator(arguments)

	if arguments.is_input_warc():
		process_warc_file(verticalizator, arguments.get_input(), output_file)
	elif arguments.is_input_wiki():
		process_wiki_file(verticalizator, arguments.get_input(), output_file)
	elif arguments.is_input_html():
		process_html_file(verticalizator, arguments.get_input(), output_file)

	output_file.close()
