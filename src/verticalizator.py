# -*- coding: utf-8 -*-

# Author: Miloš Švaňa (xsvana01)
# Brno 2016

from justextcpp import justext, keep_everything, set_justext_modifier, set_stoplist_path
from nltk import sent_tokenize
from tokenizer import TreebankWordTokenizer
from htmlpreprocess import HtmlPreprocessor
from urlparse import urljoin
import langid
import re
import string

#-----
# Simple justext modifier that changes the final class of all paragraphs containing links to 
# english wikipedia to 'good' so they will not be removed 
#-----

wikipedia_link_re = re.compile('http[s]?\:\/\/en\.wikipedia\.org', re.IGNORECASE)

def justext_wikilinks_modifier(text, html, cls):
	if cls == 'good':
		return (cls, text)
	else:
		wiki_links = wikipedia_link_re.search(html)
		if wiki_links:
			return ('good', text)
	return (cls, text)


tokenize = TreebankWordTokenizer().tokenize

class Verticalizator(object):

	def __init__(self, stoplist_path):
		print("setting stoplist path")
		set_stoplist_path(stoplist_path)
		set_justext_modifier(justext_wikilinks_modifier)
		print("stoplist path set")
		self.can_log = False
		self.can_langdetect = False
		self.count = 0

	def allow_log(self):
		self.can_log = True

	def allow_langdetect(self):
		self.can_langdetect = True

	def keep_everything(self):
		keep_everything(True)

	def verticalize(self, webpage):
		self.webpage = webpage
		if self.webpage.title:
			self.webpage.title = self.webpage.title.decode('utf-8', 'replace')
		self.webpage.payload = self.webpage.payload.decode('utf-8', 'replace')
		self.log('Processing webpage: %s' % webpage.uri)
		if not self.validate_webpage(): 
			return False
		payload = webpage.payload.replace('\0', '')
		self.log('Removing boilerplate')
		paragraphs = justext(payload)
		if len(paragraphs) == 0:
			self.log('Everything removed by justext')
			return False
		if self.can_langdetect and self.detect_language(paragraphs) != 'en':
			return False
		vert = self.build_vert(paragraphs)
		return vert

	def validate_webpage(self):
		if len(self.webpage.payload) < 50:
			self.log('Payload too short')
			return False
		if not self.webpage.is_html and self.webpage.payload[:14] != '<!DOCTYPE html':
			self.log('Does not look like HTML webpage')
			return False
		return True

	def build_vert(self, paragraphs):
		self.log('Creating doc and head')
		vert = self.build_start_vert()
		self.log('Processing individual paragraphs')
		for paragraph in paragraphs:
			if len(paragraph) == 0: continue
			vert = '%s%s' % (vert, self.build_paragraph_vert(paragraph))
		vert += '</doc>\r\n'
		return vert

	def build_start_vert(self):
		vert = '<doc url="%s"' % self.webpage.uri
		if self.webpage.title:
			title = self.clear_token(self.webpage.title)
			vert += ' title="' + title + '"'
		vert += '>\r\n'
		if self.webpage.title:
			vert += '<head>\r\n'
			vert += self.tokenize_and_verticalize(self.webpage.title)
			vert += '</head>\r\n'
		return vert

	def detect_language(self, paragraphs):
		self.log('Detecting webpage language')
		webpage_text = ''.join(paragraphs)
		lang = langid.classify(webpage_text)
		self.log('Detected language: %s' % lang[0])
		return lang[0]

	def build_paragraph_vert(self, paragraph):
		html_preprocessor = HtmlPreprocessor(paragraph)
		paragraph = html_preprocessor.preprocess()
		paragraph_length = len(paragraph)
		sentences = sent_tokenize(paragraph)
		vert = '<p>\n'
		for sentence in sentences:
			vert += '<s>\n'
			vert += self.tokenize_and_verticalize(sentence, html_preprocessor)
			vert += '</s>\n'
		vert += '</p>\n'
		return vert.encode(errors='replace')

	def tokenize_and_verticalize(self, text, html_preprocessor=None):
		vert = ''
		offset = 0
		tokens = tokenize(text)
		if html_preprocessor:
			links = html_preprocessor.get_links()
		for token in tokens:
			token_position = text.find(token, offset)
			if offset != 0 and token_position == offset:
				vert += '<g/>\n'
			offset = token_position + len(token)
			if html_preprocessor:
				if token == "__LINK__":
					vert += self.tokenize_and_verticalize_anchor(links.pop(0))
				elif token == "__IMG__":
					vert += self.tokenize_and_verticalize_img(links.pop(0))
				else:
					vert += '%s\r\n' % self.clear_token(token)
			else:
				vert += '%s\r\n' % self.clear_token(token)
		return vert

	def tokenize_and_verticalize_anchor(self, link):
		vert = ""
		sentences = sent_tokenize(link[0])
		token_count = 0
		for sentence in sentences:
			tokens = tokenize(sentence)
			token_count += len(tokens)
			for token in tokens:
				vert += self.clear_token(token)
				if token != tokens[-1]:
					vert += '\n'
		if len(link[1]) > 0 and link[1][0] != "#":
			uri = self.build_valid_uri(link[1])
			vert += "\t<link=%s />\t<length=%d>\n" % (uri, token_count)
		else:
			vert += "\n"
		return vert

	def tokenize_and_verticalize_img(self, link):
		if len(link[1]) > 0 and link[1][0] != "#":
			vert = "__IMG__\t<link=%s />\t<length=1>\n" % self.build_valid_uri(link[1])
		else:
			vert = ""
		return vert

	def build_valid_uri(self, uri):
		absolute_url = urljoin(self.webpage.uri, uri)
		quoted_url = absolute_url.replace('<', '%%3C').replace('>', '%%3E')
		quoted_url = quoted_url.replace('|', '%%7C').replace(' ', '%20')             
		return quoted_url

	def clear_token(self, token):
		''' 
		Makes sure token does not cause trouble when processed by other 
		programs like indexer:
		- replaces vertical bar with broken bar
		- removes control characters and other unwanted characters
		- HTML escapes < and > characters
		'''
		token = filter(lambda x: x > 31, token)
		token = token.replace(u'\u3000', '').replace(u'\u2007', '')
		token = token.replace('\n', ' ')
		token = token.replace('<', "&lt;").replace('>', '&gt;')
		if type(token) is str:
			token = token.decode('utf8', 'replace')
		token = token.replace('|', u'¦').replace(u'|', u'¦')
		return token

	def log(self, message):
		if self.log: 
			print(message)

