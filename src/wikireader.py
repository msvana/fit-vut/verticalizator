# -*- coding: utf8 -*-

# Author: Miloš Švaňa (xsvana01)
# Brno 2016

from warcreader import Webpage
import codecs
import re

class WikiReader(object):
	uri_and_title_re = re.compile(r'url=\"([^\"]+)\"\ title\=\"([^\"]+)\"')

	def __init__(self, filename):
		self.open(filename)

	def iterate(self):
		in_payload = False
		payload = '<div>'
		uri = title = ''
		for line in self.file_obj:
			if in_payload:
				if line[:6] == '</doc>':
					in_payload = False
					wiki_page = Webpage(uri, payload + '</div>', True)
					wiki_page.title = title
					yield wiki_page
					payload = '<div>'
				else:
					payload += line
			elif line[:4] == '<doc':
				uri, title = self.get_uri_and_title(line)
				in_payload = True

	def open(self, filename):
		self.file_obj = codecs.open(filename, "r")
		return self.file_obj

	def get_uri_and_title(self, line):
		matches = self.uri_and_title_re.search(line)
		return matches.groups()

